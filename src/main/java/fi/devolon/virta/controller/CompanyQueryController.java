package fi.devolon.virta.controller;

import fi.devolon.virta.exception.NotFoundException;
import fi.devolon.virta.model.dto.CompanyDto;
import fi.devolon.virta.model.dto.Response;
import fi.devolon.virta.model.queries.GetCompanyChildrenQuery;
import fi.devolon.virta.model.queries.GetCompanyQuery;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static fi.devolon.virta.common.ErrorMessage.NOT_FOUND;
import static org.springframework.http.HttpStatus.OK;

@RestController
public class CompanyQueryController {

    private final QueryGateway queryGateway;

    @Autowired
    public CompanyQueryController(QueryGateway queryGateway) {
        this.queryGateway = queryGateway;
    }

    @GetMapping("/api/company/{company_id}")
    public ResponseEntity<Response<CompanyDto>> getCompany(HttpServletRequest request,
                                                           @PathVariable("company_id") String companyId) throws InterruptedException, ExecutionException {
        CompletableFuture<CompanyDto> query = queryGateway.query(new GetCompanyQuery(companyId), CompanyDto.class);
        CompanyDto companyDto = query.get();
        if (companyDto == null)
            throw new NotFoundException(NOT_FOUND);
        return new ResponseEntity<>(new Response<CompanyDto>().build()
                .setMessage(companyDto)
                .setPath(request.getRequestURI()), OK);
    }

    @GetMapping("/api/company/{company_id}/children")
    public ResponseEntity<Response<List<CompanyDto>>> getCompanyChildren(HttpServletRequest request,
                                                                         @PathVariable("company_id") String companyId) throws InterruptedException, ExecutionException {
        List<CompanyDto> companies = queryGateway.query(new GetCompanyChildrenQuery(companyId),
                ResponseTypes.multipleInstancesOf(CompanyDto.class)).get();
        if (companies == null)
            throw new NotFoundException(NOT_FOUND);
        return new ResponseEntity<>(new Response<List<CompanyDto>>().build()
                .setMessage(companies)
                .setPath(request.getRequestURI()), OK);
    }

}

