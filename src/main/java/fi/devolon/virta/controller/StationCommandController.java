package fi.devolon.virta.controller;

import fi.devolon.virta.model.commands.station.CreateStationCommand;
import fi.devolon.virta.model.commands.station.DeleteStationCommand;
import fi.devolon.virta.model.dto.Response;
import fi.devolon.virta.model.dto.StationDto;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

import static org.springframework.http.HttpStatus.OK;

@RestController
public class StationCommandController {

    private final CommandGateway commandGateway;

    @Autowired
    public StationCommandController(CommandGateway commandGateway) {
        this.commandGateway = commandGateway;
    }

    @PostMapping("/api/company/{company_id}/station")
    public ResponseEntity<Response<String>> addStation(HttpServletRequest request,
                                                       @PathVariable("company_id") String companyId,
                                                       @RequestBody StationDto station) {
        commandGateway.send(new CreateStationCommand(companyId,
                station.getId(),
                station.getName(),
                station.getLatitude(),
                station.getLongitude()));
        return new ResponseEntity<>(new Response<String>().build()
                .setMessage("Station created")
                .setPath(request.getRequestURI()), OK);
    }

    @DeleteMapping("/api/company/{company_id}/station/{station_id}")
    public ResponseEntity<Response<String>> deleteStation(HttpServletRequest request,
                                                          @PathVariable("company_id") String companyId,
                                                          @PathVariable("station_id") String stationId) {
        commandGateway.send(new DeleteStationCommand(companyId, stationId));
        return new ResponseEntity<>(new Response<String>().build()
                .setMessage("Station deleted")
                .setPath(request.getRequestURI()), OK);
    }

}
