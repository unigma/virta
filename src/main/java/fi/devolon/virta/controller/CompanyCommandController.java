package fi.devolon.virta.controller;

import fi.devolon.virta.model.commands.company.CreateCompanyCommand;
import fi.devolon.virta.model.commands.company.UpdateCompanyCommand;
import fi.devolon.virta.model.dto.CompanyDto;
import fi.devolon.virta.model.dto.Response;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

import java.util.Date;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
public class CompanyCommandController {

    private final CommandGateway commandGateway;

    @Autowired
    public CompanyCommandController(CommandGateway commandGateway) {
        this.commandGateway = commandGateway;
    }

    @PostMapping("/api/company")
    public ResponseEntity<Response<String>> createCompany(HttpServletRequest request,
                                                          @RequestBody CompanyDto company) {
        commandGateway.send(new CreateCompanyCommand(company.getId(),
                company.getName(),
                company.getParentId()));
        return new ResponseEntity<>(new Response<String>()
                .setStatus(201)
                .setError("Created")
                .setTimestamp(new Date())
                .setMessage("Company created")
                .setPath(request.getRequestURI()), CREATED);
    }

    @PutMapping("/api/company/{company_id}")
    public ResponseEntity<Response<String>> updateCompany(HttpServletRequest request,
                                                          @PathVariable("company_id") String companyId,
                                                          @RequestBody CompanyDto company) {
        commandGateway.send(new UpdateCompanyCommand(companyId, company.getName()));
        return new ResponseEntity<>(new Response<String>().build()
                .setMessage("Company updated")
                .setPath(request.getRequestURI()), OK);
    }

}
