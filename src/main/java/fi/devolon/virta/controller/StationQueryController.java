package fi.devolon.virta.controller;

import fi.devolon.virta.exception.NotFoundException;
import fi.devolon.virta.model.dto.Response;
import fi.devolon.virta.model.dto.StationDto;
import fi.devolon.virta.model.queries.GetCompanyStationsQuery;
import fi.devolon.virta.model.queries.GetStationsByDistanceQuery;
import fi.devolon.virta.model.queries.GetStationsByIdsQuery;
import fi.devolon.virta.model.queries.GetStationQuery;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static fi.devolon.virta.common.ErrorMessage.NOT_FOUND;
import static org.springframework.http.HttpStatus.OK;

@RestController
public class StationQueryController {

    private final QueryGateway queryGateway;

    @Autowired
    public StationQueryController(QueryGateway queryGateway) {
        this.queryGateway = queryGateway;
    }

    @GetMapping("/api/station/{station_id}")
    public ResponseEntity<Response<StationDto>> getStation(HttpServletRequest request,
                                                           @PathVariable("station_id") String stationId) throws InterruptedException, ExecutionException {
        CompletableFuture<StationDto> query = queryGateway.query(new GetStationQuery(stationId), StationDto.class);
        StationDto stationDto = query.get();
        if (stationDto == null)
            throw new NotFoundException(NOT_FOUND);
        return new ResponseEntity<>(new Response<StationDto>().build()
                .setMessage(stationDto)
                .setPath(request.getRequestURI()), OK);
    }

    @GetMapping("/api/company/{company_id}/stations")
    public ResponseEntity<Response<List<StationDto>>> getCompanyStations(HttpServletRequest request,
                                                                         @PathVariable("company_id") String companyId) throws InterruptedException, ExecutionException {
        List<String> stationIds = queryGateway.query(new GetCompanyStationsQuery(companyId),
                ResponseTypes.multipleInstancesOf(String.class)).get();
        if (stationIds == null)
            throw new NotFoundException(NOT_FOUND);
        List<StationDto> stations = queryGateway.query(new GetStationsByIdsQuery(stationIds),
                ResponseTypes.multipleInstancesOf(StationDto.class)).get();
        return new ResponseEntity<>(new Response<List<StationDto>>().build()
                .setMessage(stations)
                .setPath(request.getRequestURI()), OK);
    }

    @GetMapping("/api/station/{latitude}/{longitude}/{distance}")
    public ResponseEntity<Response<List<StationDto>>> getStationByDistance(HttpServletRequest request,
                                                                           @PathVariable("latitude") Double centerLatitude,
                                                                           @PathVariable("longitude") Double centerLongitude,
                                                                           @PathVariable("distance") Integer distance) throws InterruptedException, ExecutionException {
        List<StationDto> stations = queryGateway.query(new GetStationsByDistanceQuery(centerLatitude, centerLongitude, distance),
                ResponseTypes.multipleInstancesOf(StationDto.class)).get();
        return new ResponseEntity<>(new Response<List<StationDto>>().build()
                .setMessage(stations)
                .setPath(request.getRequestURI()), OK);
    }

}
