package fi.devolon.virta.common;

public class ErrorMessage {

    public static final String BAD_REQUEST = "Bad request";
    public static final String NOT_FOUND = "Not found";

}
