package fi.devolon.virta.util;

import com.google.common.base.Strings;
import fi.devolon.virta.exception.BadRequestException;
import fi.devolon.virta.model.commands.company.CreateCompanyCommand;
import fi.devolon.virta.model.commands.station.CreateStationCommand;
import fi.devolon.virta.model.commands.station.DeleteStationCommand;
import fi.devolon.virta.model.commands.company.UpdateCompanyCommand;

import static fi.devolon.virta.common.ErrorMessage.BAD_REQUEST;

public class Validator {

    public static void validate(CreateStationCommand cmd) {
        if (Strings.isNullOrEmpty(cmd.getCompanyId()) ||
                Strings.isNullOrEmpty(cmd.getStationId()) ||
                Strings.isNullOrEmpty(cmd.getName()) ||
                cmd.getLatitude() == null ||
                cmd.getLongitude() == null)
            throw new BadRequestException(BAD_REQUEST);
    }

    public static void validate(CreateCompanyCommand cmd) {
        if (Strings.isNullOrEmpty(cmd.getCompanyId()) ||
                Strings.isNullOrEmpty(cmd.getName()))
            throw new BadRequestException(BAD_REQUEST);
    }

    public static void validate(UpdateCompanyCommand cmd) {
        if (Strings.isNullOrEmpty(cmd.getCompanyId()) ||
                Strings.isNullOrEmpty(cmd.getName()))
            throw new BadRequestException(BAD_REQUEST);
    }

    public static void validate(DeleteStationCommand cmd) {
        if (Strings.isNullOrEmpty(cmd.getStationId()))
            throw new BadRequestException(BAD_REQUEST);
    }

}
