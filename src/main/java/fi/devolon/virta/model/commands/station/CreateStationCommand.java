package fi.devolon.virta.model.commands.station;

import org.axonframework.modelling.command.TargetAggregateIdentifier;

import lombok.Data;

@Data
public class CreateStationCommand {

	@TargetAggregateIdentifier
	private final String companyId;
	private final String stationId;
	private final String name;
	private final Double latitude;
	private final Double longitude;

}
