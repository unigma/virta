package fi.devolon.virta.model.commands.company;

import lombok.Data;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

@Data
public class UpdateCompanyCommand {

    @TargetAggregateIdentifier
    private final String companyId;
    private final String name;

}
