package fi.devolon.virta.model.commands.company;

import org.axonframework.modelling.command.TargetAggregateIdentifier;

import lombok.Data;

@Data
public class CreateCompanyCommand {

	@TargetAggregateIdentifier
	private final String companyId;
	private final String name;
	private final String parentId;

}
