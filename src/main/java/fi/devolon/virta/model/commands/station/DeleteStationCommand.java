package fi.devolon.virta.model.commands.station;


import lombok.Data;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

@Data
public class DeleteStationCommand {

    @TargetAggregateIdentifier
    private final String companyId;
    private final String stationId;

}
