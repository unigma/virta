package fi.devolon.virta.model.events;

import org.axonframework.modelling.command.TargetAggregateIdentifier;

import lombok.Data;

@Data
public class StationCreatedEvent {

	@TargetAggregateIdentifier
	private final String companyId;
	private final String stationId;
	private final String name;
	private final Double latitude;
	private final Double longitude;

}
