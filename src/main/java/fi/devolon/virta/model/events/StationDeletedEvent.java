package fi.devolon.virta.model.events;

import lombok.Data;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

@Data
public class StationDeletedEvent {

    @TargetAggregateIdentifier
    private final String companyId;
    private final String stationId;

}
