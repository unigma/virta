package fi.devolon.virta.model.events;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

@Data
@AllArgsConstructor
public class CompanyUpdatedEvent {

    @TargetAggregateIdentifier
    private String companyId;
    private String name;

}
