package fi.devolon.virta.model.events;

import lombok.Data;

@Data
public class CompanyCreatedEvent {

	private final String companyId;
	private final String name;
	private final String parentId;

}
