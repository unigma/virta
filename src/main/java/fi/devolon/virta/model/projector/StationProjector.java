package fi.devolon.virta.model.projector;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import fi.devolon.virta.exception.NotFoundException;
import fi.devolon.virta.model.entity.StationEntity;
import fi.devolon.virta.model.events.StationCreatedEvent;
import fi.devolon.virta.model.events.StationDeletedEvent;
import fi.devolon.virta.model.queries.GetStationsByDistanceQuery;
import fi.devolon.virta.model.queries.GetStationsByIdsQuery;
import fi.devolon.virta.model.queries.GetStationQuery;
import fi.devolon.virta.model.repository.StationRepo;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.queryhandling.QueryHandler;
import fi.devolon.virta.model.dto.StationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static fi.devolon.virta.common.ErrorMessage.NOT_FOUND;

@Service
public class StationProjector {

	private final StationRepo stationRepo;

	@Autowired
	public StationProjector(StationRepo stationRepo) {
		this.stationRepo = stationRepo;
	}

	@EventHandler
	public void addStation(StationCreatedEvent event) {
		StationEntity station = new StationEntity();
		station.setStationId(event.getStationId());
		station.setCompanyId(event.getCompanyId());
		station.setName(event.getName());
		station.setLatitude(event.getLatitude());
		station.setLongitude(event.getLongitude());
		stationRepo.save(station);
	}

	@EventHandler
	public void deleteStation(StationDeletedEvent event) {
		Optional<StationEntity> optional = stationRepo.findById(event.getStationId());
		optional.ifPresent(stationRepo::delete);
	}

	@QueryHandler
	public StationDto getStation(GetStationQuery query) {
		Optional<StationEntity> optional = stationRepo.findById(query.getId());
		return optional.map(this::toStation).orElse(null);
	}

	@QueryHandler
	public List<StationDto> getStationsByDistance(GetStationsByDistanceQuery query) {
		return stationRepo.findByDistance(
				query.getCenterLatitude(),
				query.getCenterLongitude(),
				query.getDistance()).stream().map(this::toStation).collect(Collectors.toList());
	}

	@QueryHandler
	public List<StationDto> getStationsByIds(GetStationsByIdsQuery query) {
		return stationRepo.findByStationIdIn(query.getIds()).stream().map(this::toStation).collect(Collectors.toList());
	}

	private StationDto toStation(StationEntity stationEntity) {
		StationDto station = new StationDto();
		station.setId(stationEntity.getStationId());
		station.setName(stationEntity.getName());
		station.setCompanyId(stationEntity.getCompanyId());
		station.setLatitude(stationEntity.getLatitude());
		station.setLongitude(stationEntity.getLongitude());
		return station;
	}

}
