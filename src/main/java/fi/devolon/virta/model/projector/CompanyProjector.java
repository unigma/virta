package fi.devolon.virta.model.projector;

import fi.devolon.virta.exception.NotFoundException;
import fi.devolon.virta.model.dto.CompanyDto;
import fi.devolon.virta.model.entity.CompanyEntity;
import fi.devolon.virta.model.events.*;
import fi.devolon.virta.model.queries.GetCompanyChildrenQuery;
import fi.devolon.virta.model.queries.GetCompanyQuery;
import fi.devolon.virta.model.queries.GetCompanyStationsQuery;
import fi.devolon.virta.model.repository.CompanyRepo;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.queryhandling.QueryHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static fi.devolon.virta.common.ErrorMessage.NOT_FOUND;

@Service
public class CompanyProjector {

    private final CompanyRepo companyRepo;

    @Autowired
    public CompanyProjector(CompanyRepo companyRepo) {
        this.companyRepo = companyRepo;
    }

    @EventHandler
    public void addCompany(CompanyCreatedEvent event) {
        CompanyEntity company = new CompanyEntity();
        company.setCompanyId(event.getCompanyId());
        company.setName(event.getName());
        company.setParentId(event.getParentId());
        companyRepo.save(company);
        addChildToParent(event.getParentId(), event.getCompanyId());
    }

    private void addChildToParent(String parentId, String childId) {
        if (parentId != null) {
            Optional<CompanyEntity> optional = companyRepo.findById(parentId);
            if (optional.isPresent()) {
                CompanyEntity parent = optional.get();
                parent.addChild(childId);
                companyRepo.save(parent);
                addChildToParent(parent.getParentId(), childId);
            }
        }
    }

    @EventHandler
    public void updateCompany(CompanyUpdatedEvent event) {
        Optional<CompanyEntity> optional = companyRepo.findById(event.getCompanyId());
        if (optional.isPresent()) {
            CompanyEntity company = new CompanyEntity();
            company.setName(event.getName());
            companyRepo.save(company);
        }
    }

    @EventHandler
    public void addStation(StationCreatedEvent event) {
        Optional<CompanyEntity> optional = companyRepo.findById(event.getCompanyId());
        if (optional.isPresent()) {
            CompanyEntity company = optional.get();
            company.addStation(event.getStationId());
            companyRepo.save(company);
        }
    }

    @EventHandler
    public void removeStation(StationDeletedEvent event) {
        Optional<CompanyEntity> optional = companyRepo.findById(event.getCompanyId());
        if (optional.isPresent()) {
            CompanyEntity company = optional.get();
            company.removeStation(event.getStationId());
            companyRepo.save(company);
        }
    }

    @QueryHandler
    public CompanyDto getCompany(GetCompanyQuery query) {
        Optional<CompanyEntity> optional = companyRepo.findById(query.getId());
        return optional.map(this::toCompany).orElse(null);
    }

    @QueryHandler
    public List<CompanyDto> getChildren(GetCompanyChildrenQuery query) {
        Optional<CompanyEntity> optional = companyRepo.findById(query.getId());
        return optional.map(companyEntity -> companyRepo.findAllByCompanyIdIn(
                companyEntity.getChildren()).stream().map(this::toCompany).collect(Collectors.toList()
        )).orElse(null);
    }

    @QueryHandler
    public Set<String> getChildrenIds(GetCompanyStationsQuery query) {
        Optional<CompanyEntity> optional = companyRepo.findById(query.getId());
        return optional.map(companyEntity -> getAllStations(companyEntity, companyEntity.getStations())).orElse(null);
    }

    private Set<String> getAllStations(CompanyEntity company, Set<String> stations) {
        // TODO No time but caching is much better!
        for (String childId : company.getChildren()) {
            Optional<CompanyEntity> optional = companyRepo.findById(childId);
            optional.ifPresent(companyEntity -> stations.addAll(companyEntity.getStations()));
        }
        return stations;
    }

    private CompanyDto toCompany(CompanyEntity companyEntity) {
        CompanyDto company = new CompanyDto();
        company.setId(companyEntity.getCompanyId());
        company.setName(companyEntity.getName());
        company.setParentId(companyEntity.getParentId());
        company.setStations(companyEntity.getStations());
        return company;
    }

}
