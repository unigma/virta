package fi.devolon.virta.model.dto;

import lombok.Data;

@Data
public class StationDto {

	private String id;
	private String name;
	private String companyId;
	private Double latitude;
	private Double longitude;

}
