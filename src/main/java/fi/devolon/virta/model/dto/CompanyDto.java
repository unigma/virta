package fi.devolon.virta.model.dto;

import lombok.Data;

import java.util.Set;

@Data
public class CompanyDto {

	private String id;
	private String name;
	private String parentId;
	private Set<String> stations;

}
