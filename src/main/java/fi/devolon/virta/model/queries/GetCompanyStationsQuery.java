package fi.devolon.virta.model.queries;

import lombok.Data;

@Data
public class GetCompanyStationsQuery {

    public final String id;

}
