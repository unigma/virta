package fi.devolon.virta.model.queries;

import lombok.Data;

@Data
public class GetStationQuery {

	private final String id;

}
