package fi.devolon.virta.model.queries;

import lombok.Data;

@Data
public class GetCompanyChildrenQuery {

    private final String id;

}
