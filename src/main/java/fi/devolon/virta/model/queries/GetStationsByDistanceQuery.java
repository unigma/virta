package fi.devolon.virta.model.queries;

import lombok.Data;

@Data
public class GetStationsByDistanceQuery {

    private final Double centerLatitude;
    private final Double centerLongitude;
    private final Integer distance;

}
