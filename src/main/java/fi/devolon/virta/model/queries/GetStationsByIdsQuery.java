package fi.devolon.virta.model.queries;

import lombok.Data;

import java.util.List;

@Data
public class GetStationsByIdsQuery {

    public final List<String> ids;

}
