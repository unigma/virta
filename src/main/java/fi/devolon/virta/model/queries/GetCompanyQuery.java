package fi.devolon.virta.model.queries;

import lombok.Data;

@Data
public class GetCompanyQuery {

	private final String id;

}
