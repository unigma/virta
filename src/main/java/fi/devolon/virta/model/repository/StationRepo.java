package fi.devolon.virta.model.repository;

import java.util.List;

import fi.devolon.virta.model.entity.StationEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface StationRepo extends CrudRepository<StationEntity, String> {
	List<StationEntity> findByStationIdIn(List<String> ids);
	@Query(value = "SELECT STATION_ID, NAME, LATITUDE, LONGITUDE, COMPANY_ID, \n" +
			"( 3959 * acos( cos( radians(:latitude) ) * cos( radians( LATITUDE ) ) \n" +
			"* cos( radians( LONGITUDE ) - radians(:longitude) ) + sin( radians(:latitude) ) \n" +
			"* sin(radians(LATITUDE)) ) ) AS distance \n" +
			"FROM station \n" +
			"GROUP BY STATION_ID \n" +
			"HAVING distance < :distance \n" +
			"ORDER BY distance;",
			nativeQuery = true)
	List<StationEntity> findByDistance(@Param("latitude") Double latitude, @Param("longitude") Double longitude, @Param("distance") Integer distance);
}
