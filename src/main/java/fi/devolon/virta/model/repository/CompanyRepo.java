package fi.devolon.virta.model.repository;

import fi.devolon.virta.model.entity.CompanyEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface CompanyRepo extends CrudRepository<CompanyEntity, String> {
    List<CompanyEntity> findAllByCompanyIdIn(Set<String> ids);
}
