package fi.devolon.virta.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "station")
@Data
public class StationEntity {

	@Id
	private String stationId;

	@Column
	private String name;

	@Column
	private Double latitude;

	@Column
	private Double longitude;

	@Column
	private String companyId;

}
