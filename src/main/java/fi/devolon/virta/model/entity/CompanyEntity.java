package fi.devolon.virta.model.entity;


import lombok.Data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "company")
@Data
public class CompanyEntity {

    @Id
    private String companyId;

    @Column
    private String name;

    @Column
    private String parentId;

    @ElementCollection(fetch = FetchType.EAGER)
    Set<String> stations;

    @ElementCollection(fetch = FetchType.EAGER)
    Set<String> children;

    public void addStation(String stationId) {
        if (this.stations == null)
            this.stations = new HashSet<>();
        this.stations.add(stationId);
    }

    public void removeStation(String stationId) {
        if (this.stations == null)
            return;
        this.stations.remove(stationId);
    }

    public void addChild(String childId) {
        if (this.children == null)
            this.children = new HashSet<>();
        this.children.add(childId);
    }

    public void removeChild(String childId) {
        if (this.children == null)
            return;
        this.children.remove(childId);
    }

}
