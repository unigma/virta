package fi.devolon.virta.model.aggregate;

import fi.devolon.virta.model.commands.company.CreateCompanyCommand;
import fi.devolon.virta.model.commands.station.CreateStationCommand;
import fi.devolon.virta.model.commands.station.DeleteStationCommand;
import fi.devolon.virta.model.commands.company.UpdateCompanyCommand;
import fi.devolon.virta.model.events.*;
import fi.devolon.virta.util.Validator;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;

import java.util.HashSet;
import java.util.Set;

@Aggregate
@NoArgsConstructor
@Getter
public class Company {

	@AggregateIdentifier
	private String id;
	private String name;
	private Set<String> stations;
	private String parentId;

	@CommandHandler
	public Company(CreateCompanyCommand cmd) {
		Validator.validate(cmd);

		AggregateLifecycle.apply(new CompanyCreatedEvent(cmd.getCompanyId(),
				cmd.getName(),
				cmd.getParentId()));
	}

	@EventSourcingHandler
	private void on(CompanyCreatedEvent event) {
		this.id = event.getCompanyId();
		this.name = event.getName();
		this.parentId = event.getParentId();
		this.stations = new HashSet<>();
	}

	@CommandHandler
	public void handle(UpdateCompanyCommand cmd) {
		Validator.validate(cmd);

		AggregateLifecycle.apply(new CompanyUpdatedEvent(cmd.getCompanyId(), cmd.getName()));
	}

	@EventSourcingHandler
	public void on(CompanyUpdatedEvent event) {
		this.name = event.getName();
	}

	@CommandHandler
	public void handle(CreateStationCommand cmd) {
		Validator.validate(cmd);

		AggregateLifecycle.apply(new StationCreatedEvent(cmd.getCompanyId(),
				cmd.getStationId(),
				cmd.getName(),
				cmd.getLatitude(),
				cmd.getLongitude()));
	}

	@EventSourcingHandler
	private void on(StationCreatedEvent event) {
		stations.add(event.getStationId());
	}

	@CommandHandler
	public void handle(DeleteStationCommand cmd) {
		Validator.validate(cmd);

		AggregateLifecycle.apply(new StationDeletedEvent(this.id, cmd.getStationId()));
	}

	@EventSourcingHandler
	public void on(StationDeletedEvent event) {
		this.stations.remove(event.getStationId());
	}

}
