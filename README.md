# Project statement

The task to create a company station solution such that a company could be assigned as the parent of another.

## How to run

Since the solution needs to consider scalability it is implemented using the CQRS pattern. To run the project first download and run axon server for event sourcing.

[Link to download axon server](https://axoniq.io/product-overview/axon-server)

## Database explanation
H2 database has been added to simulate persistence layer.

## Test
I did not have enough time to create an interface so I included an export of the Postman collection in the project.
